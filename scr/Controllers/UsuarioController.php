<?php

namespace idartes\usuario\Controllers; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use idartes\usuario\Repository\ParametroDetalleRepository;
use idartes\usuario\Repository\UsuarioInterface;
use idartes\usuario\Requests\CrearUsuarioRequest;
use idartes\usuario\Requests\ActualizarUsuarioRequest; 
use idartes\usuario\Estado;
class UsuarioController extends Controller
{



    protected $usuarioRepository;
    protected $parametrosRepository;

    public function __construct(ParametroDetalleRepository $configuracion, UsuarioInterface $usuarioRepository ){
        $this->parametrosRepository=$configuracion;
        $this->usuarioRepository=$usuarioRepository;
    }  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/gestionar-usuarios'); 
    } 

    public function inicio(){
        $data = [
            'areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'areasPys'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS_PAZ_Y_SALVOS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'tipoDocumentos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_TIPO_DOCUMENTO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'ciudades'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_CIUDAD)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'generos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_GENERO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'etnias'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_ETNIA)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'eps'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_EPS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
        ]; 
        //dd($mensajes)     ;
        return view('material.sections.usuario.gestionar-usuarios',$data);         
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CrearUsuarioRequest $request) 
    { 
        
        $idUsuario=$this->usuarioRepository->crear($request);
        if($idUsuario!=-1){
            $mensajes= [
                'message'=>'El usuario ha sido creado con éxito con el código '.$idUsuario,
                'title'=>'Usuario Creado!',
                'type'=>'success'
            ];
        }else{
            $mensajes= [
                'message'=>'No se ha podido completar la creación del usuario',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];             
        } 
        return redirect('/gestionar-usuarios')->with($mensajes);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = $this->usuarioRepository->obtenerUsuarioPorId($id); 
        $data = [ 
            
            'areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'areasPys'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS_PAZ_Y_SALVOS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'tipoDocumentos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_TIPO_DOCUMENTO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'ciudades'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_CIUDAD)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'generos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_GENERO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'etnias'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_ETNIA)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'eps'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_EPS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),            
            'usuario'=> $usuario
        ]; 
        //dd($mensajes)     ;
        return view('material.sections.usuario.gestionar-usuarios',$data);            
        //$returnHTML="";        
        //$returnHTML = view('material.sections.usuario.form.edit',$data)->render(); 
        //return response()->json(array('success' => true, 'html'=>$returnHTML,'usuario'=>$usuario)); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActualizarUsuarioRequest $request, $id) 
    {
        $resultado=$this->usuarioRepository->actualizar($request,$id);
        if($resultado){
            $mensajes= [
                'message'=>'Los datos del usuario ha sido modificado con éxito',
                'title'=>'Usuario Modificado!',
                'type'=>'success'
            ];
        }else{
            $mensajes= [
                'message'=>'No se ha podido completar la edición del usuario',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];            
        } 
        return redirect('/gestionar-usuarios')->with($mensajes); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consultarUsuariosArea(Request $request){        
        $area=$request['i_fk_id_area']; 
        $u= $this->usuarioRepository->obtenerUsuariosPorArea($area);
        //dd($u);
        //return response()->json($c);  
        //$returnHTML ="";  
        $returnHTML = view('material.sections.usuario.tabla-usuarios')->with('usuarios', $u)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML,'usuarios'=>$u)); 
    }     

    public function resetPassword(Request $request){ 
        $mensaje=[]; 
        if($this->usuarioRepository->resetPassword($request['id'])){
            $mensaje=[
                'title'=>'Bien hecho',
                'message'=>'Se ha Reseteado la contraseña, ahora el usuario podrá ingresar con el documento',
                'type'=>'success'
            ];
        }
        else{
            $mensaje=[
                'title'=>'Algo ha salido mal',
                'message'=>'No ha sido posible resetear la contraseña',
                'type'=>'error'
            ];            
        }
        return response()->json($mensaje); 
    }     

    public function removerRolesUsuario(Request $request){
        $sinRoles = $this->usuarioRepository->removerRolesUsuario($request['id']);
        if($sinRoles){
            $mensaje=[
                'title'=>'Bien hecho',
                'message'=>'El usuario ha sido desactivado satisfactoriamente.',
                'type'=>'success'
            ];
        }
        else{
            $mensaje=[
                'title'=>'Algo ha salido mal',
                'message'=>'No ha sido posible desactivar el usuario',
                'type'=>'error'
            ];            
        }
        return response()->json($mensaje); 
    }
}
