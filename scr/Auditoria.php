<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;


class Auditoria extends Model
{
    
    protected $table = 'tbl_auditoria';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = ['i_id_registro', 'vc_tabla','i_fk_id_usuario','tx_campos_antes','tx_campos_ahora'];
    public $timestamps = true;

    public function usuario()
	{
		return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_usuario','id');
	}

}