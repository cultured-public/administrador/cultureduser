<?php

namespace idartes\usuario;

//use Illuminate\Database\Eloquent\Model;
use Hoyvoy\CrossDatabase\Eloquent\Model;

class ParametroDetalles extends Model
{
    protected $table = 'tbl_parametros_detalles';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = [
        'vc_parametro_detalle','i_estado','i_fk_id_parametro', 'i_fk_id_padre', 'vc_codigo_interno', 'd_rpc', 'i_codigo_interno',
    ];
    public $timestamps = true;
    protected $appends = ['vc_nombre','vc_interno_nombre'];
    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    } 

    public function usuariosciudades()
    {
    	return $this->hasMany(config('usuarios.modelo_user'), 'i_fk_ciudad','i_pk_id');
    }

    public function usuariosgeneros()
    {
        return $this->hasMany(config('usuarios.modelo_user'), 'i_fk_genero','i_pk_id');
    }

    public function usuariosetnias()
    {
        return $this->hasMany(config('usuarios.modelo_user'), 'i_fk_etnia','i_pk_id');
    }

    public function usuariosepss()
    {
        return $this->hasMany(config('usuarios.modelo_user'), 'i_fk_eps','i_pk_id');
    }

     public function usuariostiposDocumentos()
    {
        return $this->hasMany(config('usuarios.modelo_user'), 'i_fk_tipo_documento','i_pk_id');
    }

    public function hijos()
    {
        return $this->hasMany('idartes\usuario\ParametroDetalles', 'i_fk_id_padre','i_pk_id');
    }

    public function padre()
    {
        return $this->belongsTo('idartes\usuario\ParametroDetalles', 'i_fk_id_padre','i_pk_id');
    }
    
    public function getVcNombreAttribute()
    {
        return strtoupper($this->vc_parametro_detalle);
    }   
    
    public function getVcInternoNombreAttribute()
    {
        return strtoupper($this->vc_codigo_interno).". ".strtoupper($this->vc_parametro_detalle);
    }  
    
    public function parametro()
    {
        return $this->belongsTo('idartes\usuario\Parametro', 'i_fk_id_padre','i_pk_id');
    }    
}  