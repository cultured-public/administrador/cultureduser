<?php

namespace idartes\usuario\GestionPerfiles\Providers;

use Illuminate\Support\ServiceProvider;
use idartes\usuario\GestionPerfiles\Repository\PerfilRepository;

class PerfilServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('idartes\usuario\GestionPerfiles\Repository\PerfilInterface', function($app)
        {
            return new PerfilRepository;
        });
    }
}
