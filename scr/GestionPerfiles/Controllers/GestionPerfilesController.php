<?php

namespace idartes\usuario\GestionPerfiles\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use idartes\usuario\GestionPerfiles\Requests\PerfilRequest;
//use idartes\usuario\GestionPerfiles\Repository\PerfilRepository;
use idartes\usuario\GestionPerfiles\Repository\PerfilInterface;
use App\Modulos\Modulo\ModuloInterface;
use DB; 

class GestionPerfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected $perfilRepository;
    protected $moduloRepository;

    public function __construct(PerfilInterface $perfilRepository, ModuloInterface $moduloRepository){
        $this->perfilRepository=$perfilRepository;
        $this->moduloRepository=$moduloRepository;
    }    

    public function index()
    {
        //
        $data = [
            'perfiles'=>$this->perfilRepository->mostrarTabla(),
            'modulo' => $this->moduloRepository->obtenerTodo()->pluck( 'vc_modulo','i_pk_id')->toArray(),
            'clase'  => 268,
        ];
        return view('material.sections.perfiles.perfiles', $data);
    }


    public function cargos()
    {
        //
        $data = [
            'perfiles'=>$this->perfilRepository->mostrarTabla(),
            'modulo' => $this->moduloRepository->obtenerTodo()->pluck( 'vc_modulo','i_pk_id')->toArray(),
            'clase'  => 269,
        ];
        return view('material.sections.perfiles.perfiles', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //dd($request);
        $vistaRetorno = ($request->i_fk_id_clase==268) ? '/perfiles' : '/cargos'; 
        if(isset($request->i_pk_id)){
            return $this->update($request,$request->i_pk_id);
        }
        
        if($this->perfilRepository->crear($request)){
            $mensajes = [
                'message'=>'El perfil ha sido creado',
                'title'=>'Éxito',
                'type'=>'success'                
            ];
            return redirect($vistaRetorno)->with($mensajes);
        }else{
            $mensajes = [
                'message'=>'El perfil NO ha sido creado',
                'title'=>'Error',
                'type'=>'error'                
            ];  
            return redirect($vistaRetorno)->with($mensajes)->withInput();          
        }
        /*Tipo::create($request->all());
        dd($request);
        return redirect()->route('material.sections.perfiles.perfiles')
                        ->with('success','Perfil creado exitosamente');*/

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = [
            'perfiles'=>$this->perfilRepository->mostrarTabla(),
            'modulo' => $this->moduloRepository->obtenerTodo()->pluck( 'vc_modulo','i_pk_id')->toArray(),
            'perfil' => $this->perfilRepository->obtener($id,[]),
            'clase'  => 268,
        ];        
        return view('material.sections.perfiles.perfiles', $data);

    }


    public function editCargos($id)
    { 

        $data = [
            'perfiles'=>$this->perfilRepository->mostrarTabla(),
            'modulo' => $this->moduloRepository->obtenerTodo()->pluck( 'vc_modulo','i_pk_id')->toArray(),
            'perfil' => $this->perfilRepository->obtener($id,[]),
            'clase'  => 269,
        ];        
        return view('material.sections.perfiles.perfiles', $data);

    }    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vistaRetorno = ($request->i_fk_id_clase==268) ? '/perfiles' : '/cargos';   
        if($this->perfilRepository->actualizar($request,$id)){
            $mensajes = [
                'message'=>'El perfil ha sido modificado',
                'title'=>'Éxito',
                'type'=>'success'                
            ];
            return redirect($vistaRetorno)->with($mensajes);
        }else{
            $mensajes = [
                'message'=>'El perfil NO ha sido modificado',
                'title'=>'Error',
                'type'=>'error'                
            ];  
            return redirect($vistaRetorno)->with($mensajes)->withInput();          
        }

    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $this->perfilRepository->eliminar($id);
        //dd($id);
        return view('/perfiles')->with('success', 'Perfil eliminado satisfactoriamente');
    }
}
