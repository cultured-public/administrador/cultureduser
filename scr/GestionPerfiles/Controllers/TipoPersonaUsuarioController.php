<?php

namespace idartes\usuario\GestionPerfiles\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use idartes\usuario\GestionPerfiles\Requests\TipoActividadRequest;
use idartes\usuario\Tipo;
use idartes\usuario\Modulo;
use idartes\usuario\Actividad;
use idartes\usuario\User;
use DB; 

class TipoPersonaUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


 

    public function index()
    {
        //
        ;

        $data= [
            'usuarios'=>User::where('vc_estado',1)->get()->pluck('full_name','id')->toArray(),
            'user' => null,
            'clase'=>268 
        ];        

        return view('material.sections.perfiles.tipopersona-usuario',$data);
    }


    public function cargos()
    {
        //
        ;

        $data= [
            'usuarios'=>User::where('vc_estado',1)->get()->pluck('full_name','id')->toArray(),
            'user' => null,
            'clase'=>269  
        ];        

        return view('material.sections.perfiles.tipopersona-usuario',$data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        
        return null;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        //$request['tipoPersona'];
        $data= [
            'usuarios'=>User::where('vc_estado',1)->get()->pluck('full_name','id')->toArray(),
            'user'=>User::with('tiposPersona')->find($request->user_id),   
            'tipoPersonas'=>Tipo::where('i_fk_id_clase',$request->i_fk_id_clase)->with('modulos')->get(),
            'clase'=>$request->i_fk_id_clase
        ];
        //$user = User::with('tiposPersona')->where('i_cedula',$request['i_cedula'])->first();
        //$tipoPersonas = Tipo::where('i_fk_id_clase',$request->i_fk_id_clase)->get(); 
        
        return view('material.sections.perfiles.tipopersona-usuario',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
      
    }

    public function add(Request $request,$id_tipo_persona,$id_usuario_hiden)
    {
        $user = User::find($id_usuario_hiden);
        $user->tiposPersona()->attach($id_tipo_persona,[
                        'i_estado'=>1,
                    ]);
      
        return "Bien !!";
    }

    public function remove(Request $request,$id_tipo_persona,$id_usuario_hiden)
    {
        $user = User::find($id_usuario_hiden);

        $user->tiposPersona()->detach($id_tipo_persona);
      
        return "Eliminada !!";
    }
}
