<?php

namespace idartes\usuario\GestionPerfiles\Requests;

use Illuminate\Foundation\Http\FormRequest;


class TipoActividadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'tipoPersona' => 'required',
            'modulo' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'tipoPersona.required' => 'El tipo persona es requerido',
            'modulo.required' => 'El modulo es requerido',
        ];
    }
}
