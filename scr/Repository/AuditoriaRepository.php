<?php

namespace idartes\usuario\Repository; 
use Auth;
use Illuminate\Support\Facades\DB;

class AuditoriaRepository{

	public static function setUserId($conexion){
		$pdo = DB::connection($conexion)->getPdo();
		$stmt=$pdo->prepare("SET @user_id = :user_id ;");
		$user_id=Auth::user()->id;
		$stmt->bindParam(":user_id",$user_id); 
		$stmt->execute(); 		
	}

}