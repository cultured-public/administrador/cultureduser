<?php

namespace idartes\usuario\Repository;
use idartes\usuario\User;
use idartes\usuario\TipoPersona;
use Auth; 
use Illuminate\Support\Facades\DB;
use idartes\usuario\Repository\AuditoriaRepository as Aud;
use idartes\usuario\Repository\UsuarioInterface;
class UsuarioRepository implements UsuarioInterface{

	public function obtenerUsuarioPorId($id){
		return User::find($id);		
	}

	public function obtenerUsuarioPorCedula($documento){
		return User::where('i_cedula',$documento)->first();		
	}

	public function crear($request){
		//Para auditoría
		Aud::setUserId('baseadmin'); 
		$usuario = new User();
		$data = $request->only($usuario->getFillable());
		$data['vc_estado'] = 1; 
		$data['password'] = bcrypt($data['i_cedula']);
		if($usuario->fill($data)->save()){
			return $usuario->id;
		}else{
			return -1;
		}
	}

	public function actualizar($request,$id){
		//Para auditoría
		Aud::setUserId('baseadmin'); 		
		$usuario = User::find($id);
		$data = $request->only($usuario->getFillable());
		$data['vc_estado'] = 1; 
		return $usuario->fill($data)->save();
	}

	public function obtenerUsuariosPorArea($areas){ 
		return User::whereIn('i_fk_area',$areas)->with('area')->get();  	 
	}  

	public function obtenerUsuariosActivos(){
		return User::where('vc_estado',1)->get()->pluck('fullname','id')->toArray();
	}
	
	public function obtener($id, $relaciones = []){}
	public function eliminar($id){}
	public function obtenerTodo($relaciones = []){}
	public function dataTable($relaciones = []){}	

    public function resetPassword($id){        
        $usuario = User::find($id);
		$usuario->password = bcrypt($usuario->i_cedula);
		return $usuario->save();
	} 	
	
	public function removerRolesUsuario($id)
	{	
		$sinRoles = 0;
		$usuario = User::find($id);
		if(is_object($usuario)){
			$usuario->vc_estado = 0;
			if($usuario->save()){
				TipoPersona::where('user_id',$id)->delete();
				$sinRoles = 1;
			}
		}
		return $sinRoles;
	}
}