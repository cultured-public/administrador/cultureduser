<?php

namespace idartes\usuario\Repository;

use idartes\usuario\Repository\CRUDInterface;

interface UsuarioInterface extends CRUDInterface
{
	public function obtenerUsuarioPorId($id);
	public function obtenerUsuarioPorCedula($documento); 
	public function obtenerUsuariosPorArea($areas);
	public function obtenerUsuariosActivos();
}