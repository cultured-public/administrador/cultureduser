<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;


class TipoPersona extends Model
{
    
    protected $table = 'tbl_tipo_persona';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = ['i_estado', 'user_id', 'tipo_id','d_fecha_inicio','d_fecha_fin'];
  
    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    }
    
    public function usuario()
    {
        return $this->belongsTo(config('usuarios.modelo_user') ,'user_id','id');
    }

    public function tipo()
    {
        return $this->belongsTo(config('usuarios.modelo_tipo') ,'tipo_id','i_pk_id');
    }


}
