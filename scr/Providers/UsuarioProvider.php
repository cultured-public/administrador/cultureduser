<?php 

namespace idartes\usuario\Providers;

use Illuminate\Support\ServiceProvider;
use idartes\usuario\Repository\UsuarioInterface;
use idartes\usuario\Repository\UsuarioRepository;

class UsuarioProvider extends ServiceProvider
{

	public function boot()

	{

	}

	public function register()
	{
        $this->app->bind(UsuarioInterface::class, function($app)
        {
            return new UsuarioRepository;
        });
	}

}
