<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;


class Parametro extends Model
{
    protected $table = 'tbl_parametros';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = [
        'vc_parametro','i_estado','i_codigo_interno'
    ];
    public $timestamps = true;
    
    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    } 
    public function detalles()
    {
    	return $this->hasMany('idartes\usuario\ParametroDetalles', 'i_fk_id_parametro','i_pk_id');
    }
}