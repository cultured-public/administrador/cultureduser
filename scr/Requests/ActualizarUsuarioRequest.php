<?php

namespace idartes\usuario\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
class ActualizarUsuarioRequest extends FormRequest
{
    //protected $errorBag = 'actualizar';  
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array 
     */
    public function rules(Request $request) 
    {	
        return [ 
            'i_cedula'=>'required|unique:baseadmin.tbl_users,i_cedula,'.$request->route()->parameters['usuario'], 
            //'email'=>'required|email|unique:baseadmin.tbl_users,email,'.$request->route()->parameters['usuario'],
			'i_fk_tipo_documento'=>'required',
			'name'=>'required',
			'vc_primer_apellido'=>'required',
			//'dt_fecha_nacimiento'=>'required', 
			//'vc_rh'=>'required',
            'i_fk_genero'=>'required',
            'i_fk_eps'=>'required',
            'i_fk_ciudad'=>'required', 
        ];
    }    
 
    public function messages()
    {
        return [
            'i_cedula.unique' => 'El documento de identidad ya se encuentra registrado.', 
            //'email.unique' => 'El correo ya se encuentra registrado.',
			'i_fk_tipo_documento'=>'Campo obligatorio',
			'name'=>'Campo obligatorio',
			'vc_primer_apellido'=>'Campo obligatorio',
			'dt_fecha_nacimiento'=>'Campo obligatorio', 
			//'vc_rh'=>'Campo obligatorio',
            'i_fk_genero'=>'Campo obligatorio',
            'i_fk_eps'=>'Campo obligatorio',
            'i_fk_ciudad'=>'Campo obligatorio',             
        ];
    }       

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */

   /* protected function failedValidation(Validator $validator)
    {
        $this->session()->flash('form', 'actualizar'); 
        throw (new ValidationException($validator))
                    //->errorBag('crear')
                    ->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    }    */       
}
